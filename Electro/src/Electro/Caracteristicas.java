package Electro;

import javax.swing.JOptionPane;

public class Caracteristicas extends Electro {
private String disponible;
private String marca;
private String presio;
private String grantia;
private String Pantalla;
private String resolucion;
private String modelo;
private String color;
private String lanzamiento;

public Caracteristicas(
        String Almacen, 
        String Electro, 
        String Funcion, 
        String modelo, 
        String color, 
        String lanzamiento, 
        String resolucion, 
        String Pantalla, 
        String grantia,
        String presio,
        String marca,
        String disponible){
    
    super (Almacen,Electro,Funcion);
    this.modelo = modelo;
    this.color = color;
    this.lanzamiento = lanzamiento;  
    this.resolucion = resolucion; 
    this.Pantalla = Pantalla;
    this.grantia = grantia;
    this.presio = presio;
    this.marca = marca;
    this.disponible = disponible;
}
public void mostarDatos(){
    JOptionPane.showMessageDialog(null,
            "ALMACEN:  "+getAlmacen()+
            "\nELECTRODOMETICO:  "+getElectro()+
            "\nFUNCION:  "+getFuncion()+
            "\nRESOLUCION:  "+resolucion+
            "\nCOLOR:  "+color+
            "\nMODELO:  "+modelo+
            "\nLANZAMIENTO:  "+lanzamiento+
            "\nPATANTALLA TACTIL:  "+Pantalla+
            "\nGRANTIA:  "+grantia+
            "\nPRESIO:  "+presio+
            "\nMARCA:  "+marca+
            "\nUNIDADES DISPONIBLES:  "+disponible       
              );
}
}