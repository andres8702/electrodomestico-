package Electro;

public class Electro {
       
    private String Almacen;
    private String Electro;
    private String Funcion;
    
    public String getAlmacen() {
        return Almacen;
    }

    public String getElectro() {
        return Electro;
    }

    public String getFuncion() {
        return Funcion;
    }

    public Electro(String Almacen, String Electro, String Funcion) {
        this.Almacen = Almacen;
        this.Electro = Electro;
        this.Funcion = Funcion;
    }
            
}
